//
// Created by ata on 7/7/17.

#include "../Headers/loginwindow .h"
LoginWindow::LoginWindow(QWidget *parent)
        : QWidget(parent)
{
    QRect rec = QApplication::desktop() -> screenGeometry();
    setGeometry(rec.width() / 2 - 512 / 2, rec.height() / 2 - 256 / 2, 512, 256);
    //set layout

    login=new QPushButton("login",this);
    exit=new QPushButton("exit",this);
    //two push button named login and exit
    isBot=new QCheckBox("Bot",this);
    isObserver=new QCheckBox("Observer",this);
    connect(login,&QPushButton::clicked,this,&LoginWindow::Login);
    connect(exit,&QPushButton::clicked,this,&LoginWindow::close);
    //first line:if login button is clicked then slot Login will be called
    //second line: if exit button is clicked then program will be terminated

    QVBoxLayout box1;//this is a vertical layout and its elements from top to button is username server address server port fields and login and exit buttons
    box1.addWidget(&username);
    box1.addWidget(&address);
    box1.addWidget(&port);
    box1.addWidget(login);
    box1.addWidget(exit);
    box1.addWidget(isBot);
    box1.addWidget(isObserver);
    //adding elements to the box1

    setLayout(&box1);
    show();
    //set window
}

void LoginWindow::Login()
{

    emit Login_signal(username.text(),QHostAddress(address.text()),port.text().toUShort(),isBot->isChecked(),isObserver->isChecked());
    hide();
}//if this slot is called then login_signal will be emitted and then login menu window will be hide

//

