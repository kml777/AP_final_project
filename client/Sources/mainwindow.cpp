//
// Created by ata on 7/8/17.
//
#include "../Headers/mainwindow.h"


MainWindow::MainWindow(bool bot,bool observer,QWidget *parent):QWidget(parent),isBot(bot),isObserver(observer)
{

    flag=0;//set flag 0 this means you are in game

    otherObjects.clear();
    myCircle.clear();
    size.clear();
    x.clear();
    y.clear();
    virus.clear();
    cr.clear();

    //set mouse setting first line set mouse tracking without clicking
    //second line sets up the event filter for events related to rhe mouse
    this->setMouseTracking(true);
    qApp->installEventFilter(this);

    //gets screen geometry
    rec = QApplication::desktop()->screenGeometry();

    /*
     * first line set seed for random numbers
     * second line build a circle
     * and the last line brush this cirlce
     */
    std::srand(std::time(0));
    myCircle.push_back(new QGraphicsEllipseItem(0,0,50*(!isObserver),50*(!isObserver)));
    myCircle.back()->setBrush(QBrush(Qt::red,Qt::SolidPattern));
    myCircle[0]->moveBy((rand()%2000)*(!isObserver),(rand()%2000)*(!isObserver));
    /*
     * first line set scene geometry
     * second adding circle
     * and then adding scene to the view
     */
    scene.setSceneRect(rec.width()/4,rec.height()/4,rec.width()/2,rec.height()/2);
    //vi.setSceneRect(rec.width()/4,rec.height()/4,rec.width()/2,rec.height()/2);
    //this->showMaximized();

    scene.addItem(myCircle.back());
    vi.setScene(&scene);

    // show and maximizing view
    vi.showMaximized();
    vi.show();

    //initializing circle data

    size.push_back(50*(!isObserver));
    y.push_back((myCircle.back()->y())*(!isObserver));
    x.push_back(myCircle.back()->x()*(!isObserver));

    //every 100ms moves circle according to the mouse position
    connect(&time,&QTimer::timeout,this,&MainWindow::move);
    time.start(300);

    //send initial position to the server
    sendChange();
}


bool MainWindow::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::MouseMove) {
        QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);
        mouseMoveEvent(keyEvent);
        return true;
    }
    if(event->type()==QEvent::MouseButtonPress)
    {
        QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);
        mousePressEvent(keyEvent);
        return true;
    }
    return false;
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    mx=event->x();
    my=event->y();
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::RightButton)
    {
        sendChange(0,1);
    }
    else if(event->button()==Qt::LeftButton)
    {

        int moveX = (mx - (rec.x() + rec.width()) / 2) /50, moveY = (my - (rec.y() + rec.height()) / 2) / 50;
        int cnt=0,sz=myCircle.size();
        for(int i=0;i<sz&&cnt<16;i++)
            if(size[i]/2>50)
            {
                cnt++;
                int t=size[i],ml=abs(rand()%16),sgnX=moveX/std::max(abs(moveX),1),sgnY=moveX/std::max(abs(moveY),1);
                size[i]/=2;
                size.push_back(t/2+t%2);
                x.push_back(x[i]+moveX*ml+(size[i]+size.back())*sgnX);
                y.push_back(y[i]+moveY*ml+(size[i]+size.back())*sgnY);
                //TODO handle intersect
                sendChange();
            }

    }

}

int pw(int a)
{
    return a*a;
}

void MainWindow::move()
{
    int mass=0;
    for(int i=0;i<size.size();i++)
        mass+=size[i];
    mass=std::max(mass,50);
    int moveX=(mx-(rec.x()+rec.width())/2)/(7*(int)sqrt(mass)),moveY=(my-(rec.y()+rec.height())/2)/(7*(int)sqrt(mass));
    //cerr<<moveX<<" "<<moveY<<"\n";
    // cerr<<"!!!"<<isBot<<"\n";
    /*if(myCircle->x()+moveX<0||myCircle->x()+moveX>rec.width()-70)
        moveX=0;
    if(myCircle->y()+moveY<0||myCircle->y()+moveY>rec.height()-50)
        moveY=0;*/
    if((*std::min_element(x.begin(),x.end())<=-4000&&moveX<0)||(*std::max_element(x.begin(),x.end())>=4000&&moveX>0))
        moveX=0;
    if((*std::min_element(y.begin(),y.end())<=-4000&&moveY<0)||(*std::max_element(y.begin(),y.end())>=4000&&moveY>0))
        moveY=0;
    //cerr<<"in move function::\n";
    if(!isBot)
        for(int i=0;i<myCircle.size();i++)
        {
            x[i] += moveX, y[i] += moveY;
//        cerr << "myCircle->x:" << x[i] << " myCircle->y:" << y[i] << "\n";
        }
    else
    {
        cerr<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
        QList<QPair<int,Client> > smaller;
        smaller.clear();
        for(int i=0;i<cr.size();i++)
            for(int j=0;j<size.size();j++)
                if(cr[i].second.size<size[j])
                    smaller.push_back(qMakePair(j,cr[i].second));
        int dist=1e9;
        for(int i=0;i<smaller.size();i++)
            dist=std::min(dist,pw(x[smaller[i].first]-smaller[i].second.x)+pw(y[smaller[i].first]-smaller[i].second.y));
        for(int i=0;i<smaller.size();i++)
            if(dist==pw(x[smaller[i].first]-smaller[i].second.x)+pw(y[smaller[i].first]-smaller[i].second.y))
            {
                if(x[smaller[i].first]<smaller[i].second.x)
                    for(int j=0;j<size.size();j++)
                        x[j]+=std::min(17*7/(int)sqrt(mass),abs(x[smaller[i].first]-smaller[i].second.x));
                else
                    for(int j=0;j<size.size();j++)
                        x[j]-=std::min(17*3/(int)sqrt(mass),abs(x[smaller[i].first]-smaller[i].second.x));
                if(y[smaller[i].first]<smaller[i].second.y)
                    for(int j=0;j<size.size();j++)
                        y[j]+=std::min(8*7/(int)sqrt(mass),abs(y[smaller[i].first]-smaller[i].second.y));
                else
                    for(int j=0;j<size.size();j++)
                        y[j]-=std::min(8*7/(int)sqrt(mass),abs(y[smaller[i].first]-smaller[i].second.y));
                break;
            }
       // cerr<<dist<<"\n";

    }
    sendChange();
    // cerr<<"end of move function\n\n";

}


void MainWindow::newChange(QString s)
{
    cerr<<"in new change fuction:\n";
    cerr<<s.toStdString()<<"\n";
    if(flag)return;
    bool firstTime=1;
    const QString init=QString::fromStdString("init");
    for(int i=0;i<init.length();i++)
        if(init[i]!=s[i])
            firstTime=0;
    if(firstTime)
    {
        initialize(s);
        return;
    }
    if(virus.size()==0)
    {
        cerr<<"!!!";
        sendChange(1, 0);
        return ;
    }

    //scene.clear();
    for(int i=0;i<otherObjects.size();i++)
        delete(otherObjects[i]);
    otherObjects.clear();

    for(int i=0;i<myCircle.size();i++)
        delete myCircle[i];
    myCircle.clear();
    size.clear();
    x.clear();
    y.clear();
   // std::cerr<<"!!!"<<myCircle.size()<<"\n";

    scene.clear();

    if(s.toStdString()=="0")
    {
        if(!flag)
        {
            disconnect(&time, &QTimer::timeout, this, &MainWindow::move);
            flag=1;
        }
        return;
    }

    QString t="";

    cr.clear();

    char c;

    for(int i=0;i<s.length();i++)
        if(s[i]=="\n")
        {
            Client tmp;
            std::istringstream input(t.toStdString());
            input>>c>>tmp.size>>tmp.x>>tmp.y;
            if(c=='s')
                cr.push_back(qMakePair(0,tmp));
            else if(c=='c')
                cr.push_back(qMakePair(1,tmp));
            else if(c=='f')
                cr.push_back(qMakePair(2,tmp));
            t="";
        }
        else
            t += s[i];

    int x1,y1;
    for(int i=0;i<cr.size();i++)
        if(!cr[i].first)
        {
            x.push_back(cr[i].second.x);
            y.push_back(cr[i].second.y);
            size.push_back(cr[i].second.size);

            myCircle.push_back(new QGraphicsEllipseItem(0, 0, size.back(), size.back()));
            myCircle.back()->moveBy(x.back(),y.back());
            if(!i)
                x1=rec.x()+rec.width()/2-myCircle[0]->x(),y1=rec.y()+rec.height()/2-myCircle[0]->y();
            myCircle.back()->setBrush(QBrush(Qt::red, Qt::SolidPattern));
            myCircle.back()->moveBy(x1, y1);
            scene.addItem(myCircle.back());
        }
        else
        {
            otherObjects.push_back(new QGraphicsEllipseItem(0,0,cr[i].second.size,cr[i].second.size));
            otherObjects.back()->moveBy(cr[i].second.x,cr[i].second.y);
            if(cr[i].first==1)
                otherObjects.back()->setBrush(QBrush(Qt::blue, Qt::SolidPattern));
            else if(cr[i].first==2)
                otherObjects.back()->setBrush(QBrush(Qt::black, Qt::SolidPattern));
            scene.addItem(otherObjects.back());
            otherObjects.back()->moveBy(x1,y1);
            //         cerr<<cr.size()<<"foregin object:: x:"<<otherObjects.back()->x()<<" y:"<<otherObjects.back()->y()<<"\n";
        }

    //  cerr<<virus.size()<<"\n";
    for(int i=0;i<virus.size();i++)
    {
        otherObjects.push_back(new QGraphicsEllipseItem(0,0,100,100));
        otherObjects.back()->moveBy(virus[i].first,virus[i].second);
        otherObjects.back()->setBrush(QBrush(Qt::green, Qt::SolidPattern));
        scene.addItem(otherObjects.back());
        otherObjects.back()->moveBy(x1,y1);
    }
   // cerr<<size.size()<<" "<<myCircle.size()<<"\n";
   // cerr<<"receivd string:"<<s.toStdString()<<"\n";
    cerr<<virus.size()<<"\n";
    cerr<<"end of new change function\n\n";
}
//a slot which activated when new message received maybe own message but in this case notification sound doesn't play

void MainWindow::sendChange(bool firstTime,bool throwFood)
{

    std::ostringstream os;
    if(firstTime)
    {
        os<<"init";
        emit(changePos(QString::fromStdString(os.str())));

        return;
    }
    if(throwFood)
    {
        for(int i=0;i<size.size();i++)
            if(size[i]>100)
            {
                int moveX = (mx - (rec.x() + rec.width()) / 2) / 50, moveY = (my - (rec.y() + rec.height()) / 2) / 50;
                //          cerr << "//////////////////////////////////////////////////////\n";
                //        cerr << "mx is::" << mx << " my is::" << my << "\n";
                //      cerr << "//////////////////////////////////////////////////////\n";
                size[i] -= 50;
                os << "f" << x[i] + moveX * 50 << " " << y[i] + moveY * 50 << "\n";
                break;
            }
    }
    for(int i=0;i<size.size();i++)
        os<<size[i]<<" "<<x[i]<<" "<<y[i]<<"\n";
    os<<"0 0 0\n";
  //   cerr<<"send change function::string sending:"<<os.str()<<"\n";
    QString s=QString::fromStdString(os.str());
    emit changePos(s);//signal to interface
    //cerr<<"end of send change function\n\n";
}

void MainWindow::initialize(QString s) {
    std::istringstream input(s.toStdString());
    string t;
    input >> t;
    for (int i = 0; i < 10; i++) {
        int x, y;
        input >> x >> y;
        virus.push_back(qMakePair(x, y));
    }

}


