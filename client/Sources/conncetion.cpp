//
// Created by ata on 7/8/17.
//

#include "../Headers/connection.h"

connection::connection(QHostAddress address,quint16 port)
{
    sc.connectToHost(address,port);
    connect(&sc,&QTcpSocket::readyRead,this,&connection::recive_message);
}

connection::~connection()
{
    sc.write(QString::fromStdString("close").toUtf8());
    sc.close();
    sc.disconnectFromHost();
}

void connection::recive_message()
{
    QString s=sc.readAll();
    emit newChange(s);
}

void connection::sendPos(QString s)
{
    sc.write(s.toUtf8());
}