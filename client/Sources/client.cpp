//
/// / Created by ata on 7/15/17.

#include "../Headers/client.h"


client::client()
{
    lw=new LoginWindow();
    connect(lw,&LoginWindow::Login_signal,this,&client::startGame);
}

void client::startGame(QString name, QHostAddress add, quint16 prt,bool bot,bool observer)
{
    cn=new connection(add,prt);
    mw=new MainWindow(bot,observer);
    connect(cn,&connection::newChange,mw,&MainWindow::newChange);
    connect(mw,&MainWindow::changePos,cn,&connection::sendPos);
}

client::~client()
{
    delete cn;
    delete mw;
    delete lw;
}

//

