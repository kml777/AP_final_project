//
// Created by ata on 7/15/17.
//

#ifndef CLIENT_CLIENT_H
#define CLIENT_CLIENT_H

#include"loginwindow .h"
#include"mainwindow.h"
#include"connection.h"

class client:public QObject
{
Q_OBJECT
public:
    client();
    ~client();
public slots:
    void startGame(QString ,QHostAddress,quint16,bool,bool);
private:
    LoginWindow *lw;
    MainWindow *mw;
    connection *cn;

};


#endif //CLIENT_CLIENT_H
