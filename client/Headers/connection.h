//
// Created by ata on 7/8/17.
//

#ifndef CLIENT_CONNECTION_H
#define CLIENT_CONNECTION_H

#include <QObject>
#include <QHostAddress>
#include <QTcpSocket>


class connection:public QObject
{
Q_OBJECT
public:
    connection(QHostAddress , quint16);
    ~connection();
private:
    QTcpSocket sc;

public slots:
    void recive_message();
    void sendPos(QString);
signals:
    void newChange(QString);
};


#endif //CLIENT_CONNECTION_H
