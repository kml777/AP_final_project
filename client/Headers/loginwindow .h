//
// Created by ata on 7/7/17.
//

#ifndef CLIENT_LOGINWINDOW_H
#define CLIENT_LOGINWINDOW_H

#include<QObject>
#include<QApplication>
#include<QWidget>
#include<QDesktopWidget>
#include<QMainWindow>
#include<QVBoxLayout>
#include<QHBoxLayout>
#include<QLabel>
#include<QLineEdit>
#include<QPushButton>
#include<QHostAddress>
#include <QtWidgets/QCheckBox>

class LoginWindow:public QWidget
{
Q_OBJECT
public:
    LoginWindow(QWidget* =0);
    QCheckBox *isBot;
    QCheckBox *isObserver;

private:
    QLineEdit username,address,port;
    QPushButton *login,*exit;

signals:
    void Login_signal(QString,QHostAddress =QHostAddress("127.0.0.1"),quint16 =8000,bool =0,bool =0);
    void botMod();
public slots:
    void Login();
};


#endif //CLIENT_LOGINWINDOW_H
