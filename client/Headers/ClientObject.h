//
// Created by ata on 7/14/17.
//

#ifndef CLIENT_CLIENTOBJECT_H
#define CLIENT_CLIENTOBJECT_H
#include<QObject>
#include<QApplication>
#include<QByteArray>
#include<string>
#include<math.h>

using std::string;

class Client:public QObject
{
public:
   // Client(QByteArray);
    Client();
    Client(const Client&);
    int size,x,y;
    bool operator <(const Client &second)const
    {
        if(size==second.size)
        {
            if(x==second.x)
                return y<second.y;
            return x<second.x;
        }
        return size<second.size;
    }
    Client &operator = (const Client &second)
    {
        x=second.x,size=second.size,y=second.y;
        return *this;
    }
};

#endif //CLIENT_CLIENTOBJECT_H
