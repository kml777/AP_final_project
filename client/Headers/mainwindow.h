//
// Created by ata on 7/8/17.
//

#ifndef CLIENT_MAINWINDOW_H
#define CLIENT_MAINWINDOW_H

#include <QPushButton>
#include <QWidget>
#include <QApplication>
#include <QDesktopWidget>
#include <QTextEdit>
#include <QHBoxLayout>
#include <QLabel>
#include <QCheckBox>
#include <QMediaPlayer>
#include <QShortcut>
#include <QFileInfo>
#include<QGraphicsEllipseItem>
#include<QTimer>
#include<QGraphicsScene>
#include<QGraphicsView>
#include<QtWidgets/QPlainTextEdit>
#include <iostream>
#include <sstream>
#include "ClientObject.h"
#include<algorithm>

using std::cerr;

class MainWindow:public QWidget
{
Q_OBJECT
public:
    MainWindow(bool, bool ,QWidget * =NULL);
public slots:
    void newChange(QString );//works in interface
    void sendChange(bool =false,bool =false);
    void move();
    void mouseMoveEvent(QMouseEvent*);
    bool eventFilter(QObject *,QEvent *);
    void mousePressEvent(QMouseEvent *);
private:
    QString name;
    QList<QGraphicsEllipseItem *> myCircle,otherObjects;
    QList<QPair<int,int> > virus;
    QGraphicsScene scene;
    QGraphicsView vi;
    QTimer time;
    double mx,my;
    QRect rec;
    QList<int> size,x,y;
    bool flag,isBot,isObserver;
    QList<QPair<int,Client> > cr;
    void initialize(QString);

signals:
    void changePos(QString );

};


#endif //CLIENT_MAINWINDOW_H
