cmake_minimum_required(VERSION 3.7)
project(client)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_PREFIX_PATH /home/ata/qt/5.8/gcc_64)

find_package(Qt5 REQUIRED COMPONENTS Core Widgets Gui Network Multimedia)

set(SOURCE_FILES main.cpp "Headers/loginwindow .h" Sources/loginwindow.cpp Headers/connection.h Sources/conncetion.cpp Headers/mainwindow.h Sources/mainwindow.cpp Headers/ClientObject.h Sources/ClientObject.cpp Headers/client.h Sources/client.cpp)
add_executable(client ${SOURCE_FILES})

target_link_libraries(client Qt5::Core Qt5::Widgets Qt5::Gui Qt5::Network Qt5::Multimedia)