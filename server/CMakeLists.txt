cmake_minimum_required(VERSION 3.7)
project(server)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_PREFIX_PATH /home/ata/qt/5.8/gcc_64)

find_package(Qt5 REQUIRED COMPONENTS Core Widgets Gui Network Multimedia)

set(SOURCE_FILES main.cpp Headers/server.h Sources/server.cpp Headers/clientObject.h Sources/clientObject.cpp)
add_executable(server ${SOURCE_FILES})

target_link_libraries(server Qt5::Core Qt5::Widgets Qt5::Gui Qt5::Network Qt5::Multimedia)