//
// Created by ata on 6/30/17.
#include <iostream>
#include <cstdlib>
#include"../Headers/server.h"

bool q(QPair<int,Client> f,QPair<int,Client> l)
{
    return l.second<f.second;
}

Server::Server()
{
    lastId=0;
    server=new QTcpServer;

    client.clear();
    clients.clear();

    QHostAddress local=QHostAddress("127.0.0.1");
    server->listen(local,8000);

    connect(server,&QTcpServer::newConnection,this,&Server::newClient);

    num=flag=0;

    connect(&time,&QTimer::timeout,this,&Server::makeTrue);
    time.start(10);

    srand(std::time(0));

    food.clear();
    for(int i=0;i<40;i++)
        food.push_back(qMakePair(rand()%4000,rand()%4000));

    virus.clear();
    for(int i=0;i<10;i++)
        virus.push_back(qMakePair(rand()%4000,rand()%4000));
}

void Server::makeTrue()
{flag=1;}

void Server::newClient()
{
    QTcpSocket *nc=server->nextPendingConnection();
    clients.push_back(qMakePair(lastId++,nc));
    Client emp;
    client.push_back(emp);
    connect(nc,&QTcpSocket::readyRead,this,&Server::newChange);
}

int pw(int a)
{
    return a*a;
}

void Server::init(int index)
{
    cerr<<index<<"\n";
    std::ostringstream v;
    QString s;
    s = QString::fromStdString("init\n");

    for (int j = 0; j < virus.size(); j++)
        v << virus[j].first << " " << virus[j].second << "\n";

    s += QString::fromStdString(v.str());
    clients[index].second->write(s.toUtf8());
    cerr<<s.toStdString()<<"\n";
    num++;
}

void Server::newChange()
{

    // cerr<<num<<" "<<clients.size()<<"\n";
    for(int i=0;i<clients.size();i++)
        if(clients[i].second->isValid()&&clients[i].second->bytesAvailable())
        {
            QByteArray p=clients[i].second->readAll();
            string s=p.toStdString();
            if(s=="close")
            {
                cerr<<"!!!!!!";
                for (int j = clients[i].first+1;j < client.size();j++)
                    std::swap(client[j],client[j-1]);
                client.pop_back();
                for(int j=0;j<clients.size();j++)
                    if(clients[j].first>i)
                        clients[j].first--;
                for(int j=i+1;j<clients.size();j++)
                    std::swap(clients[j],clients[j-1]);
                clients.pop_back();
                i--;
                continue;
            }
            std::istringstream foodInput(s);
            //cerr<<"newChange function:: client "<<i+1<<" of "<<client.size()<<": "<<s<<"\n";
            if(s=="init")
                init(i);
            else if(s[0]=='f')
            {
                int x1,y1;
                char c;
                foodInput>>c>>x1>>y1;
                int x,y,size,pos=0;
                do {
                    foodInput >> size >> x >> y;
                    if(!x&&!y&&!size)
                        break;
                    if (pos == client[clients[i].first].size.size())
                    {
                        client[clients[i].first].size.push_back(0);
                        client[clients[i].first].x.push_back(0);
                        client[clients[i].first].y.push_back(0);
                    }
                    client[clients[i].first].size[pos] = size, client[clients[i].first].x[pos] = x, client[clients[i].first].y[pos++] = y;
                }
                while(x||y||size);
                food.push_back(qMakePair(x1,y1));
                flag=1;
            }
            else
            {
                int x,y,size,pos=0;
                do {
                    foodInput >> size >> x >> y;
                    if(!x&&!y&&!size)
                        break;
                    if (pos == client[clients[i].first].size.size())
                    {
                        client[clients[i].first].size.push_back(0);
                        client[clients[i].first].x.push_back(0);
                        client[clients[i].first].y.push_back(0);
                    }
                    client[clients[i].first].size[pos] = size, client[clients[i].first].x[pos] = x, client[clients[i].first].y[pos++] = y;
                }
                while(x||y||size);
            }
        }

    for(int i=0;i<client.size();i++)
    {
        int num=client[i].size.size();
        for (int l = 0; l < num; l++)
            for (int j = 0; j < virus.size(); j++)
                if (pw(client[i].x[l] - virus[j].first) + pw(client[i].y[l] - virus[j].second) - pw(client[i].size[l] + 100) / 3 < 0) {
                    if (client[i].size[l] > 100) {
                        flag=1;
                        for (int k = 0; k < std::min(16, client[i].size[l] / 50 - 1); k++) {
                            client[i].size[l] -= 50;
                            client[i].size.push_back(50);
                            client[i].x.push_back(client[i].x[l] + rand() % 50 + (50 + client[i].size[l]));
                            client[i].y.push_back(client[i].y[l] + rand() % 50 + (50 + client[i].size[l]));
                        }
                    }
                }
    }

    for(int i=0;i<client.size();i++)
        for(int l=0;l<client[i].size.size();l++)
            if(client[i].size[l])
                for(int j=0;j<food.size();j++)
                    if(pw(client[i].x[l]-food[j].first)+pw(client[i].y[l]-food[j].second)-pw(client[i].size[l]+20)<0)
                    {
                        client[i].size[l]+=5;
                        //   cerr<<1<<"\n";
                        flag=1;
                        if(j<40)
                            food[j]=qMakePair(rand()%2000,rand()%2000);
                        else
                        {
                            swap(food[j],food.back());
                            food.pop_back();
                            j--;
                        }
                    }

    /* QList<QPair<int,Client> > tmp;
     tmp.clear();

     for(int i=0;i<client.size();i++)
         tmp.push_back(qMakePair(i,client[i]));
     for(int k=0;k<tmp.size();k++)
     {
         for (int i = 0; i < tmp.size(); i++)
             for(int l=0;l<tmp[i].second.size.size();l++)
                 if (tmp[i].second.size[l] != 0)
                     for (int j = 0; j < tmp.size(); j++)
                         if(i!=j)
                             for(int r=0;r<tmp[j].second.size.size();r++)
                                 if (tmp[i].second.size[l]>tmp[j].second.size[r]&&tmp[j].second.size[r]!=0&&pw(tmp[i].second.x[l] - tmp[j].second.x[r]) + pw(tmp[i].second.y[l] - tmp[j].second.y[r]) <= pw(tmp[i].second.size[l] + tmp[j].second.size[r]))
                                 {
                                     flag=1;
                                     tmp[i].second.size[l]+=tmp[j].second.size[r];
                                     tmp[j].second.size[r] = 0;

                                     int z=tmp[j].second.size[r];
                                     tmp[j].second.size[r]=tmp[j].second.size.back();
                                     tmp[j].second.size.back()=z;

                                     z=tmp[j].second.x[r];
                                     tmp[j].second.x[r]=tmp[j].second.size.back();
                                     tmp[j].second.x.back()=z;

                                     z=tmp[j].second.y[r];
                                     tmp[j].second.y[r]=tmp[j].second.size.back();
                                     tmp[j].second.y.back()=z;

                                     tmp[j].second.size.pop_back();
                                     tmp[j].second.x.pop_back();
                                     tmp[j].second.y.pop_back();
                                 }
     }//###if too slow delete this for*/

    for(int k=0;k<client.size();k++)
        for(int i=0;i<client.size();i++)
            for(int l=0;l<client[i].size.size();l++)
                if(client[i].size[l])
                    for(int j=0;j<client.size();j++)
                        if(i!=j)
                            for(int r=0;r<client[j].size.size();r++)
                                if(client[j].size[r]&&client[i].size[l]>client[j].size[r])
                                    if(pw(client[i].x[l]-client[j].x[r])+pw(client[i].y[l]-client[j].y[r])-pw(client[i].size[l]+client[j].size[r])/3<0)
                                    {
                                        client[i].size[l]+=client[j].size[r];
                                        client[j].size[r]=0;
                                        std::swap(client[j].size[r],client[j].size.back());
                                        std::swap(client[j].x[r],client[j].x.back());
                                        std::swap(client[j].y[r],client[j].y.back());
                                        client[j].y.pop_back();
                                        client[j].x.pop_back();
                                        client[j].size.pop_back();
                                        flag=1;
                                    }



    if(flag)
    {
        flag=0;
        /* for (int i = 0; i < tmp.size(); i++)
             client[tmp[i].first] = tmp[i].second;*/
        for(int i=0;i<clients.size();i++)
            if(!clients[i].second->isValid()||!clients[i].second->isOpen())
            {
                cerr<<"!!!!!!";
                for (int j = clients[i].first+1;j < client.size();j++)
                    std::swap(client[j],client[j-1]);
                client.pop_back();
                for(int j=0;j<clients.size();j++)
                    if(clients[j].first>i)
                        clients[j].first--;
                for(int j=i+1;j<clients.size();j++)
                    std::swap(clients[j],clients[j-1]);
                clients.pop_back();
                i--;
            }
        for (int i = 0; i < client.size(); i++)
        {
            if (client[i].size.size() == 0)
            {
                QString s = "0";
                for (int j = 0; j < clients.size(); j++)
                    if (clients[j].first == i)
                    {
                        clients[j].second->write(s.toUtf8());
                        clients[j].second->close();
                        int id=clients[j].first;
                        for(int k=i+1;k<client.size();k++)
                            std::swap(client[k],client[k-1]);
                        client.pop_back();
                        for(int k=j+1;k<clients.size();k++)
                            std::swap(clients[k],clients[k-1]);
                        clients.pop_back();
                        for(int k=0;k<clients.size();k++)
                            if(clients[k].first>id)
                                clients[k].first--;
                        break;
                    }
            }
            if(i==client.size())break;
            std::ostringstream os,q,f,v;
            os.clear();
            for(int l=0;l<client[i].size.size();l++)
                os <<"s"<< client[i].size[l] << " " << client[i].x[l] << " " << client[i].y[l] << "\n";
            //  cerr<<"!!!!!"<<client[i].size.size()<<"????"<<os.str()<<"\n";
            QString s = QString::fromStdString(os.str());
            //cerr<<"initial string is:"<<s.toStdString()<<"\n";
            //cerr<<"client size is:"<<client.size()<<"\n";
            for (int j = 0; j < client.size(); j++)
                if (j != i)
                {
                    //cerr<<"loop::j:"<<j<<" i:"<<i<<"\n";
                    for(int l=0;l<client[j].size.size();l++)
                        q<<"c"<< client[j].size[l] << " " << client[j].x[l] << " " << client[j].y[l] << "\n";
                    // cerr<<"q::"<<q.str()<<"\n";
                }

            s += QString::fromStdString(q.str());
            for(int j=0;j<food.size();j++)
                f<<"f"<<20<<" "<<food[j].first<<" "<<food[j].second<<"\n";
            s+=QString::fromStdString(f.str());



            //cerr<<"string sending to client number "<<i+1<<" is:"<<s.toStdString()<<"\n";
            for (int j = 0; j < clients.size(); j++)
                if (clients[j].first == i)
                {
                    clients[j].second->write(s.toUtf8());
                    break;
                }
        }

    }
}

//end of project summer starts!

