//
// Created by ata on 7/2/17.
//

#ifndef SERVER_CLIENTOBJECT_H
#define SERVER_CLIENTOBJECT_H

#include<QObject>
#include<QApplication>
#include<QByteArray>
#include<string>
#include<math.h>

using std::string;

class Client:public QObject
{
public:
    //Client(QByteArray);
    Client();
    Client(const Client&);
    QList<int>size,x,y;
    bool operator <(const Client &second)const
    {
        if(size==second.size)
        {
            if(x==second.x)
                return y<second.y;
            return x<second.x;
        }
        return size<second.size;
    }
    Client &operator = (const Client &second)
    {
        x=second.x,size=second.size,y=second.y;
        return *this;
    }
};


#endif //SERVER_CLIENTOBJECT_H
