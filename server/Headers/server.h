//
// Created by ata on 6/30/17.
//

#ifndef SERVER_SERVER_H
#define SERVER_SERVER_H

#include<QTcpServer>
#include<QTcpSocket>
#include <QJsonDocument>
#include<QList>
#include<QObject>
#include<QApplication>
#include "clientObject.h"
#include<algorithm>
#include<sstream>
#include<QTimer>
#include<iostream>
#include<cstdlib>
#include<random>

using std::cerr;
using std::uniform_int_distribution;

class Server:public QObject
{
Q_OBJECT
public:
    Server();
private:
    QTcpServer *server;
    QList<QPair<int,QTcpSocket*> > clients;
    QList<Client> client;
    int lastId,num;
    bool flag;
    QTimer time;
    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution,distribution1;
    QList<QPair<int,int> > food;
    QList<QPair<int,int> > virus;
public slots:
    void newClient();
    void newChange();
    void makeTrue();
    void init(int);
};


#endif //SERVER_SERVER_H
